require 'colorize'
require 'singleton'
require 'forwardable'

require_relative 'lib/program'
require_relative 'lib/config'

require_relative 'lib/helpers'
require_relative 'lib/helpers/prompter'

require_relative 'lib/plateau/plateau'
require_relative 'lib/plateau/place_rover'
require_relative 'lib/plateau/field'

require_relative 'lib/rover/rover'
require_relative 'lib/rover/heading'
require_relative 'lib/rover/builder'
require_relative 'lib/rover/move'
require_relative 'lib/rover/navigation/ask_about_instructions'

require_relative 'lib/rover/navigation/factory'
require_relative 'lib/rover/navigation/abstract_navigation'
require_relative 'lib/rover/navigation/transactional_navigation'
require_relative 'lib/rover/navigation/standard_navigation'

require_relative 'lib/stages/abstract_stage'

require_relative 'lib/stages/plateau_stage'
require_relative 'lib/stages/plateau_stage/ask_about_dimensions'

require_relative 'lib/stages/rover_stage'
require_relative 'lib/stages/rover_stage/ask_about_position'

require_relative 'lib/stages/reading_stage'

require_relative 'lib/validators/regexp_validator'

require_relative 'lib/errors/application_error'
require_relative 'lib/errors/invalid_input_error'
require_relative 'lib/errors/undefined_field_error'
require_relative 'lib/errors/taken_field_error'
