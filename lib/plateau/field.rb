class Plateau
  class Field
    attr_accessor :rover
    attr_reader :x, :y

    def initialize(x:, y:)
      @x, @y = x, y
    end
  end
end
