class Plateau
  attr_reader :rows, :cols

  def initialize(rows:, cols:)
    @rows, @cols = rows, cols
  end

  def place_rover(rover:, x:, y:)
    PlaceRover.call(
      field: find_field_by_position(x: x, y: y),
      rover: rover
    )
  end

  def find_field_by_position(x:, y:)
    grid.flatten.find { |field| field.x == x && field.y == y }
  end

  def copy_grid(grid)
    @grid = grid
  end

  private

  def grid
    @grid ||= Array.new(rows) do |row_index|
      Array.new(cols) { |col_index| Field.new(x: col_index, y: row_index) }
    end.reverse
  end
end
