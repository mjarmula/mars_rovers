class Plateau
  class PlaceRover
    attr_reader :field, :rover
    private :field, :rover

    def initialize(field:, rover:)
      @field, @rover = field, rover
    end

    def self.call(**args)
      new(args).call
    end

    def call
      validate_field!
      clear_previous_field if rover.field
      field.rover = rover
      rover.field = field
    end

    private

    def validate_field!
      validate_field_existence!
      valdiate_field_avability!
    end

    def validate_field_existence!
      return if field
      raise Errors::UndefinedFieldError
    end

    def valdiate_field_avability!
      return if field.rover.nil?
      raise Errors::TakenFieldError.new(field)
    end

    def clear_previous_field
      rover.field.rover = nil
    end
  end
end
