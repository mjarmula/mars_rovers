class Helpers
  class Prompter
    def ask_about(message)
      puts message.colorize(:yellow)
      STDIN.gets.chomp
    end
  end
end
