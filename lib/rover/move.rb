class Rover
  class Move
    attr_reader :rover
    private :rover

    def initialize(rover)
      @rover = rover
    end

    def self.call(rover)
      new(rover).call
    end

    def call
      rover.place_rover_on_plateau(next_position)
    end

    private

    def next_position
      available_positions.fetch(rover.heading.direction)
    end

    def available_positions
      {
        Rover::Heading::NORTH => north_direction,
        Rover::Heading::SOUTH => south_direction,
        Rover::Heading::WEST  => west_direction,
        Rover::Heading::EAST  => east_direction
      }
    end

    def north_direction
      { x: rover.field.x, y: rover.field.y + 1 }
    end

    def south_direction
      { x: rover.field.x, y: rover.field.y - 1 }
    end

    def west_direction
      { x: rover.field.x - 1, y: rover.field.y }
    end

    def east_direction
      { x: rover.field.x + 1, y: rover.field.y }
    end
  end
end
