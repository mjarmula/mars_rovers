class Rover
  extend Forwardable

  def_delegators :heading, :rotate_right, :rotate_left, :direction
  def_delegators :field, :x, :y

  attr_reader :navigation_strategy, :heading, :id
  attr_accessor :plateau, :field

  def initialize(plateau:, navigation_strategy:, heading:)
    @plateau = plateau
    @navigation_strategy = navigation_strategy
    @heading = heading
    @id = Random.new_seed.to_s[0..4]
  end

  def navigate
    navigation_strategy.navigate(self)
  end

  def move
    Move.call(self)
  end

  def place_rover_on_plateau(x:, y:)
    plateau.place_rover(rover: self, x: x, y: y)
  end

  def report
    "rover ##{id}: #{x} #{y} #{direction}"
  end
end
