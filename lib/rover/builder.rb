class Rover
  class Builder
    attr_reader :plateau, :position
    private :plateau, :position

    def initialize(plateau:, position:)
      @plateau, @position = plateau, position
    end

    def self.build(**args)
      new(args).build
    end

    def build
      configurated_rover
    end

    private

    def configurated_rover
      rover.tap do |rover|
        rover.place_rover_on_plateau(x: position.x.to_i, y: position.y.to_i)
      end
    end

    def rover
      Rover.new(
        plateau: plateau,
        navigation_strategy: navigation_strategy,
        heading: Heading.new(position.heading)
      )
    end

    def navigation_strategy
      Rover::Navigation::Factory.get(config.rover_navigation_strategy_type)
    end

    def config
      Config.instance
    end
  end
end
