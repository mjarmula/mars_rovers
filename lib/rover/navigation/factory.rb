class Rover
  class Navigation
    class Factory
      attr_reader :type
      private :type

      def initialize(type)
        @type = type
      end

      def self.get(type)
        new(type).get
      end

      def get
        defined_navigation_strategies.fetch(type)
      end

      private

      def defined_navigation_strategies
        {
          transactional: TransactionalNavigation,
          standard: StandardNavigation
        }
      end
    end
  end
end
