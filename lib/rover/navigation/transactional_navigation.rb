require 'transaction/simple'

class Rover
  class Navigation
    class TransactionalNavigation < AbstractNavigation
      def initialize(args)
        super
        @plateau = rover.plateau
        rover.extend(Transaction::Simple)
      end

      def navigate
        rover.start_transaction
        exploration_instructions.each do |exploration_instruction|
          call_rover_method(exploration_instruction)
        end
        rover.commit_transaction
      rescue Errors::ApplicationError => e
        rover.abort_transaction
        @plateau.copy_grid(rover.plateau.send(:grid))
        rover.plateau = @plateau
        puts transaction_aborted_message
        rescue_error(e)
      end

      private

      def transaction_aborted_message
        "Transaction failed, all your actions will be rolled back \n" \
        'Starting position: ' \
        "#{rover.field.x} #{rover.field.y} #{rover.heading.direction}\n"
        .colorize(:red)
      end
    end
  end
end
