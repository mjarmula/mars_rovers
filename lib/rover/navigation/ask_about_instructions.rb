class Rover
  class Navigation
    class AskAboutInstructions
      def self.call
        new.call
      end

      def call
        validate_instructions_input!
        instructions
      end

      private

      def instructions
        exploration_instructions_input.split(//)
      end

      def validate_instructions_input!
        Validators::RegexpValidator.validate!(
          subject: exploration_instructions_input,
          regexp: /^[L|R|M]+$/
        )
      end

      def exploration_instructions_input
        @rover_commands_input ||= helpers.prompter.ask_about(<<~MESSAGE)
          Please specify exploration instructions,
          it should contain not separated letters:
          R(ight), L(left), M(ove)  (Example: LLM)
        MESSAGE
      end

      def helpers
        Helpers.instance
      end
    end
  end
end
