require 'transaction/simple'

class Rover
  class Navigation
    class StandardNavigation < AbstractNavigation
      def navigate
        exploration_instructions.each do |exploration_instruction|
          call_rover_method(exploration_instruction)
        end
      rescue Errors::ApplicationError => e
        puts error_message
        rescue_error(e)
      end

      def error_message
        'You will start from positon ' \
        "#{rover.field.x} #{rover.field.y} #{rover.heading.direction} \n"
        .colorize(:red)
      end
    end
  end
end
