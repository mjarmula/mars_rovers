class Rover
  class Navigation
    class AbstractNavigation
      attr_reader :rover
      private :rover

      def initialize(rover)
        @rover = rover
      end

      def self.navigate(rover)
        new(rover).navigate
      end

      def navigate
        raise NotImplementedError, 'must implement abstract method move'
      end

      private

      def config
        Config.instance
      end

      def call_rover_method(exploration_instruction)
        rover_method(exploration_instruction).call
      end

      def rover_method(instruction)
        rover_methods_map.fetch(instruction)
      end

      def rover_methods_map
        {
          'L' => rover.method(:rotate_left),
          'R' => rover.method(:rotate_right),
          'M' => rover.method(:move)
        }
      end

      def exploration_instructions
        AskAboutInstructions.call
      end

      def rescue_error(error)
        puts error.message
        navigate
      end
    end
  end
end
