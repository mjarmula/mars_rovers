class Rover
  class Heading
    NORTH   = 'N'.freeze
    SOUTH   = 'S'.freeze
    EAST    = 'E'.freeze
    WEST    = 'W'.freeze

    attr_reader :direction

    def initialize(heading)
      @direction = heading
    end

    def rotate_left
      rotate_by(-1)
    end

    def rotate_right
      rotate_by(1)
    end

    private

    def rotate_by(num)
      @direction = directions[(direction_index + num) % directions.size]
    end

    def direction_index
      directions.index(direction)
    end

    def directions
      [NORTH, EAST, SOUTH, WEST]
    end
  end
end
