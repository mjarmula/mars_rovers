class Program
  attr_reader :init_stage
  private :init_stage

  def initialize(init_stage: Stages::PlateauStage)
    @init_stage = init_stage
  end

  def self.start(**args)
    new(args).start
  end

  def start
    init_stage.start
  end
end
