class Validators
  class RegexpValidator
    attr_reader :subject, :regexp

    def initialize(subject:, regexp:)
      @subject, @regexp = subject, regexp
    end

    def self.validate!(**args)
      new(args).validate!
    end

    def validate!
      return if subject.match(regexp)
      raise Errors::InvalidInputError
    end
  end
end
