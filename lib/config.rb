class Config
  include Singleton

  def rovers_number
    2
  end

  def rover_navigation_strategy_type
    :transactional
  end
end
