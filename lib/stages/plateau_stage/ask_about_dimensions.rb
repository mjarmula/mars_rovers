class Stages
  class PlateauStage
    class AskAboutDimensions
      def self.call
        new.call
      end

      def call
        validate_plateau_input!
        plateau_dimensions_hash
      end

      private

      def plateau_dimensions_hash
        {
          rows: plateau_dimensions.first.to_i,
          cols: plateau_dimensions.last.to_i
        }
      end

      def plateau_dimensions
        plateau_dimensions_input.split(/\s/)
      end

      def plateau_dimensions_input
        @plateau_dimensions_input ||= prompter.ask_about(<<~MESSAGE)
          Please specify plateau dimensions separated by space (Example: 5 5)
          The minimal dimensions are 1 x 1
        MESSAGE
      end

      def prompter
        helpers.prompter
      end

      def helpers
        Helpers.instance
      end

      def validate_plateau_input!
        Validators::RegexpValidator.validate!(
          subject: plateau_dimensions_input,
          regexp: /^[1-9]+\s[1-9]+$/
        )
      end
    end
  end
end
