class Stages
  class PlateauStage < AbstractStage
    attr_reader :next_stage
    private :next_stage

    def initialize(next_stage: RoverStage)
      @next_stage = next_stage
    end

    def self.start(**args)
      new(args).start
    end

    def start
      go_to_next_stage
    end

    private

    def go_to_next_stage
      next_stage.start(plateau: plateau)
    end

    def plateau
      Plateau.new(plateau_dimensions)
    end

    def plateau_dimensions
      AskAboutDimensions.call
    rescue Errors::InvalidInputError => e
      puts e.message
      plateau_dimensions
    end
  end
end
