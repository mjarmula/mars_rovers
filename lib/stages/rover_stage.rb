class Stages
  class RoverStage < AbstractStage
    attr_reader :plateau, :next_stage, :rovers
    private :plateau, :next_stage, :rovers

    def initialize(plateau:, next_stage: ReadingStage)
      @plateau = plateau
      @next_stage = next_stage
      @rovers = []
    end

    def self.start(**args)
      new(args).start
    end

    def start
      config.rovers_number.times do
        rover = new_rover
        rovers << rover
        rover.navigate
      end

      go_to_next_stage
    end

    private

    def new_rover
      Rover::Builder.build(
        plateau: plateau,
        position: rover_position
      )
    rescue Errors::TakenFieldError => e
      puts e.message
      new_rover
    end

    def rover_position
      AskAboutPosition.call(plateau)
    rescue Errors::InvalidInputError => e
      puts e.message
      rover_position
    end

    def go_to_next_stage
      next_stage.start(rovers)
    end

    def config
      Config.instance
    end
  end
end
