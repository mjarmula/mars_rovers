class Stages
  class AbstractStage
    def start
      raise NotInplementedError 'must implement abstract method start'
    end
  end
end
