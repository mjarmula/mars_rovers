class Stages
  class ReadingStage
    attr_reader :rovers
    private :rovers

    def initialize(rovers)
      @rovers = rovers
    end

    def self.start(rovers)
      new(rovers).start
    end

    def start
      rovers.each { |rover| puts rover.report }
    end
  end
end
