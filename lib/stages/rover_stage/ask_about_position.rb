class Stages
  class RoverStage
    class AskAboutPosition
      PositionStruct = Struct.new(:x, :y, :heading)

      attr_reader :plateau
      private :plateau

      def initialize(plateau)
        @plateau = plateau
      end

      def self.call(plateau)
        new(plateau).call
      end

      def call
        validate_rover_position_input!
        rover_position_structure
      end

      private

      def validate_rover_position_input!
        Validators::RegexpValidator.validate!(
          subject: rover_position_input,
          regexp: position_input_regexp
        )
      end

      def position_input_regexp
        Regexp.new("^[0-#{max_x}]\s[0-#{max_y}]\s[N|S|W|E]$")
      end

      def max_x
        plateau.cols - 1
      end

      def max_y
        plateau.rows - 1
      end

      def rover_position_structure
        PositionStruct.new(*rover_position)
      end

      def rover_position
        rover_position_input.scan(/(\d)\s(\d)\s(\w)/).flatten
      end

      def rover_position_input
        @rover_position_input ||= prompter.ask_about(<<~MESSAGE)
          Please specify rover position x, y and heading
          separated by space (Example: 0 0 N)
          position should be in plateau dimension range (#{max_x} x #{max_y})
        MESSAGE
      end

      def prompter
        helpers.prompter
      end

      def helpers
        Helpers.instance
      end
    end
  end
end
