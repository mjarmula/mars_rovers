class Errors
  class UndefinedFieldError < ApplicationError
    def message
      'Couldnt find field by specified coordinates'.colorize(:red)
    end
  end
end
