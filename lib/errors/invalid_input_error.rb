class Errors
  class InvalidInputError < ApplicationError
    def message
      'Given input is invalid, please try again.'.colorize(:red)
    end
  end
end
