class Errors
  class TakenFieldError < ApplicationError
    def initialize(field)
      super
      @field = field
    end

    def message
      "Field (#{@field.x} #{@field.y}) you try to move is already taken"
        .colorize(:red)
    end
  end
end
