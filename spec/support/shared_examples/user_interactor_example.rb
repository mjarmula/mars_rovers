RSpec.shared_examples 'a user interactor' do
  let(:helpers) { double(Helpers) }
  let(:prompter) { double(Helpers::Prompter) }

  before do
    allow(Helpers).to receive(:instance).and_return(helpers)
    allow(helpers).to receive(:prompter).and_return(prompter)
    allow(prompter).to receive(:ask_about).and_return(user_input)
  end
end
