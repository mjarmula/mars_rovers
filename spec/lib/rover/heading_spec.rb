require 'spec_helper'

RSpec.describe Rover::Heading do
  subject(:instance) { described_class.new(initial_heading) }


  describe '#rotate_left' do
    context 'when current heading is North' do
      subject { instance.rotate_left }

      let(:initial_heading) { 'N' }

      context 'when called once' do
        it 'rotate 90 degrees, to West' do
          subject
          expect(instance.direction).to eq('W')
        end
      end

      context 'when called 4 times' do
        it 'rotate 360 degrees, back to North' do
          instance.rotate_left
          instance.rotate_left
          instance.rotate_left
          instance.rotate_left
          expect(instance.direction).to eq('N')
        end
      end
    end
  end

  describe '#rotate_right' do
    context 'when current heading is North' do
      subject { instance.rotate_right }

      let(:initial_heading) { 'N' }

      context 'when called once' do
        it 'rotate 90 degrees, to East' do
          subject
          expect(instance.direction).to eq('E')
        end
      end

      context 'when called 4 times' do
        it 'rotate 360 degrees, back to North' do
          instance.rotate_right
          instance.rotate_right
          instance.rotate_right
          instance.rotate_right
          expect(instance.direction).to eq('N')
        end
      end
    end
  end
end
