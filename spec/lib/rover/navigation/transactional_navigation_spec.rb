require 'spec_helper'
require 'transaction/simple'

RSpec.describe Rover::Navigation::TransactionalNavigation do
  describe '.navigate' do
    subject(:instance) { described_class.navigate(rover) }

    let(:rover) do
      Rover.new(
        plateau: plateau,
        navigation_strategy: described_class,
        heading: heading
      )
    end
    let(:plateau) { Plateau.new(rows: 5, cols: 5) }
    let(:heading) { Rover::Heading.new('N') }

    before do
      expect(Rover::Navigation::AskAboutInstructions).to receive(:call)
        .and_return(exploration_instructions)
    end

    context 'when there is only one rover placed on 0 0 N' do
      before do
        rover.place_rover_on_plateau(x: 0, y: 0)
      end

      context 'when try to put rover into empty place in plateau boundaries' do
        let(:exploration_instructions) { ['M'] }
        let(:expected_position) { { x: 0, y: 1 } }

        it 'moves rover to given position one field north' do
          subject
          expect(rover.field.x).to eq(expected_position[:x])
          expect(rover.field.y).to eq(expected_position[:y])
        end
      end

      context 'when try to put rover into empty place out of plateau boundaries' do
        let(:exploration_instructions) { ['L', 'M'] }
        let(:expected_position) { { x: 0, y: 0 } }

        before do
          allow(STDOUT).to receive(:puts)
          allow_any_instance_of(described_class).to receive(:rescue_error)
        end

        it 'roll back rover to initial position' do
          subject
          expect(rover.field.x).to eq(expected_position[:x])
          expect(rover.field.y).to eq(expected_position[:y])
        end
      end
    end

    context 'when there are two rovers' do
      let(:rover2) do
        Rover.new(
          plateau: plateau,
          navigation_strategy: described_class,
          heading: rover_2_heading
        )
      end
      let(:rover_2_heading) { Rover::Heading.new('N') }

      before do
        rover.place_rover_on_plateau(x: 0, y: 0)
        rover2.place_rover_on_plateau(x: 0, y: 1)
        allow(STDOUT).to receive(:puts)
        allow_any_instance_of(described_class).to receive(:rescue_error)
      end

      context 'when first rover tries to take field that is already taken by rover 2' do
        let(:exploration_instructions) { ['M'] }
        let(:expected_position) { { x: 0, y: 0 } }

        it 'roll back rover to initial position' do
          expect(rover.field.x).to eq(expected_position[:x])
          expect(rover.field.y).to eq(expected_position[:y])
          subject
        end
      end
    end
  end
end
