require 'spec_helper'

RSpec.describe Rover::Navigation::AskAboutInstructions do
  include_examples 'a user interactor'

  describe '.call' do
    subject { described_class.call }

    context 'user input format' do
      let(:user_input) { 'LMR' }
      let(:expected_response) { ['L', 'M', 'R'] }

      it 'returns an array of exploration instructions' do
        is_expected.to eq(expected_response)
      end
    end

    context 'user input valdiation' do
      context 'when user input is valid' do
        let(:user_input) { 'LMR' }

        it 'doesnt throw any error' do
          expect { subject }.not_to raise_error
        end
      end

      context 'when user input is invalid - contains integers' do
        let(:user_input) { 'LM5R' }

        it 'doesnt throw any error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when user input is invalid - contains spaces' do
        let(:user_input) { ' LM R' }

        it 'doesnt throw any error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when user input is invalid - contains unexpected characters' do
        let(:user_input) { 'ABC' }

        it 'doesnt throw any error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end
    end
  end
end
