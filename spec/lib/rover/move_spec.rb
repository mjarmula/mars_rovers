require 'spec_helper'

describe Rover::Move do
  describe '.call' do
    subject { described_class.call(rover) }

    let(:rover) { double(Rover, heading: heading, field: field) }
    let(:heading) { double(Rover::Heading, direction: direction) }
    let(:field) { double(Plateau::Field, x: 0, y: 0) }

    context 'when the direction is North' do
      let(:direction) { 'N' }
      let(:north_direction) { { x: 0, y: 1 } }

      it 'returns eligible position' do
        allow(rover).to receive(:place_rover_on_plateau).with(north_direction)
        subject
      end
    end

    context 'when the direction is South' do
      let(:direction) { 'S' }
      let(:north_direction) { { x: 0, y: -1 } }

      it 'returns eligible position' do
        allow(rover).to receive(:place_rover_on_plateau).with(north_direction)
        subject
      end
    end

    context 'when the direction is West' do
      let(:direction) { 'W' }
      let(:north_direction) { { x: -1, y: 0 } }

      it 'returns eligible position' do
        allow(rover).to receive(:place_rover_on_plateau).with(north_direction)
        subject
      end
    end

    context 'when the direction is East' do
      let(:direction) { 'E' }
      let(:north_direction) { { x: 1, y: 0 } }

      it 'returns eligible position' do
        allow(rover).to receive(:place_rover_on_plateau).with(north_direction)
        subject
      end
    end
  end
end
