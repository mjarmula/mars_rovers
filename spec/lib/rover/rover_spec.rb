require 'spec_helper'

RSpec.describe Rover do
  subject(:instance) { described_class.new(rover_params) }

  let(:rover_params) do
    {
      plateau: plateau,
      navigation_strategy: navigation_strategy,
      heading: heading
    }
  end

  let(:plateau) { double(Plateau) }
  let(:navigation_strategy) do
    double(Rover::Navigation::TransactionalNavigation)
  end

  let(:heading) { double(Rover::Heading) }
  let(:plateau) { double(Plateau) }

  before do
    allow(instance).to receive(:plateau).and_return(plateau)
  end

  describe '#navigate' do
    subject { instance.navigate }

    it 'process exploration instructions' do
      expect(navigation_strategy).to receive(:navigate).with(instance)
      subject
    end
  end

  describe '#move' do
    subject { instance.move }

    it 'move rover' do
      expect(Rover::Move).to receive(:call).with(instance)
      subject
    end
  end

  describe '#place_rover_on_plateau' do
    subject { instance.place_rover_on_plateau(place_params) }

    let(:place_params) { { x: 0, y: 0 } }

    it 'delegates placing rover to plateau' do
      expect(plateau).to receive(:place_rover)
        .with(x: 0, y: 0, rover: instance)
      subject
    end
  end

  describe '#report' do
    subject { instance.report }

    let(:field) { double(Plateau::Field, x: 0, y: 0) }
    let(:rover_id) { '1' }
    let(:heading) { double(Rover::Heading, direction: direction) }
    let(:direction) { 'N' }
    let(:expected_response) { 'rover #1: 0 0 N' }

    before do
      expect(instance).to receive(:id).and_return(rover_id)
      expect(instance).to receive(:field).and_return(field).at_least(:once)
      expect(instance).to receive(:heading).and_return(heading)
    end

    it 'returns report of rovers current position' do
      is_expected.to eq(expected_response)
    end
  end
end
