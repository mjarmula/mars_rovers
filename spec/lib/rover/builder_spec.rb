require 'spec_helper'

RSpec.describe Rover::Builder do
  describe '.build' do
    subject { described_class.build(builder_params) }

    let(:builder_params) { { plateau: plateau, position: position } }
    let(:plateau) { double(Plateau) }
    let(:position) do
      double(
        Stages::RoverStage::AskAboutPosition::PositionStruct,
        x: 0,
        y: 0,
        heading: 'N'
      )
    end

    let(:config) do
      double(Config, rover_navigation_strategy_type: :transactional)
    end

    before do
      expect(Config).to receive(:instance).and_return(config)
      allow(plateau).to receive(:place_rover)
    end

    it 'returns configured rover' do
      expect(subject).to be_a(Rover)
    end
  end
end
