require 'spec_helper'

RSpec.describe Stages::RoverStage do
  describe '.start' do
    subject { described_class.start(params) }

    let(:params) { { plateau: plateau, next_stage: next_stage } }
    let(:plateau) { double(Plateau) }
    let(:next_stage) { double(Stages::ReadingStage) }
    let(:config) { double(Config, rovers_number: 1) }
    let(:rover_position) do
      double(
        Stages::RoverStage::AskAboutPosition::PositionStruct,
        x: 0,
        y: 0,
        heading: 'N'
      )
    end

    let(:builder_params) do
      {
        plateau: plateau,
        position: rover_position
      }
    end

    let(:new_rover) { double(Rover) }
    let(:rovers) { [new_rover] }

    before do
      allow(Config).to receive(:instance).and_return(config)
      allow(Rover::Builder).to receive(:build).with(builder_params)
        .and_return(new_rover)
      allow(Stages::RoverStage::AskAboutPosition).to receive(:call)
        .and_return(rover_position)
      allow(new_rover).to receive(:navigate)
      allow(next_stage).to receive(:start)
      allow_any_instance_of(described_class).to receive(:rovers)
        .and_return(rovers)
    end

    it 'creates new rover' do
      expect(Rover::Builder).to receive(:build).with(builder_params)
        .and_return(new_rover)
      subject
    end

    it 'navigates the rover' do
      expect(new_rover).to receive(:navigate)
      subject
    end

    it 'stores new rover' do
      expect { subject }.to change { rovers.size }.by(1)
    end

    it 'goes to the next stage' do
      expect(next_stage).to receive(:start)
      subject
    end
  end
end
