require 'spec_helper'

RSpec.describe Stages::PlateauStage::AskAboutDimensions do
  include_examples 'a user interactor'

  describe '.call' do
    subject { described_class.call }

    context 'returns formatted response' do
      let(:user_input) { '5 5' }
      let(:expected_response) { { rows: 5, cols: 5 } }

      before do
        allow(Validators::RegexpValidator).to receive(:validate!)
          .and_return(true)
      end

      it 'returns dimension hash' do
        is_expected.to eq(expected_response)
      end
    end

    context 'validates user input' do
      context 'when user passed valid input' do
        let(:user_input) { '5 5' }

        it 'passes validation - no error is thrown' do
          expect { subject }.not_to raise_error
        end
      end

      context 'when user passed invalid input - 0 0' do
        let(:user_input) { '0 0' }

        it 'throws invalid input error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when user passed invalid input - one integer' do
        let(:user_input) { '5' }

        it 'throws invalid input error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when user passed invalid input - 2 digits not separated by space' do
        let(:user_input) { '55' }

        it 'throws invalid input error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when user passed invalid input - 2 digits separated by space with string' do
        let(:user_input) { '5 5string' }

        it 'throws invalid input error' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end
    end
  end
end
