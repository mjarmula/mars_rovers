require 'spec_helper'

RSpec.describe Stages::PlateauStage do
  describe '.start' do
    subject { described_class.start(params) }
    let(:params) { { next_stage: rover_stage } }
    let(:plateau) { double(Plateau) }
    let(:rover_stage) { double(Stages::RoverStage) }
    let(:plateau_dimensions) { { rows: 5, cols: 5 } }

    before do
      expect(Plateau).to receive(:new).and_return(plateau)
      expect(Stages::PlateauStage::AskAboutDimensions).to receive(:call)
        .and_return(plateau_dimensions)
    end

    it 'goes to the next stage (rover stage)' do
      expect(rover_stage).to receive(:start).with(plateau: plateau)
      subject
    end
  end
end
