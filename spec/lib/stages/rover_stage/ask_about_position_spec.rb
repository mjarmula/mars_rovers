require 'spec_helper'

RSpec.describe Stages::RoverStage::AskAboutPosition do
  include_examples 'a user interactor'

  describe '.call' do
    subject { described_class.call(plateau) }

    let(:plateau) { double(Plateau, cols: 5, rows: 5) }

    context 'user input return' do
      let(:user_input) { '0 0 N' }
      let(:expected_response) do
        Stages::RoverStage::AskAboutPosition::PositionStruct.new('0', '0', 'N')
      end

      it 'returns valid strucure' do
        is_expected.to eq(expected_response)
      end
    end

    context 'user input validation' do
      context 'when input is valid' do
        let(:user_input) { '0 0 N' }

        it 'passes the input - no error is thrown' do
          expect { subject }.not_to raise_error
        end
      end

      context 'when input is invalid - one integer and valid position' do
        let(:user_input) { '0 N' }

        it 'passes the input - no error is thrown' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when input is invalid - two integers and invalid position' do
        let(:user_input) { '0 0 Z' }

        it 'passes the input - no error is thrown' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when input is invalid - no integer and valid position' do
        let(:user_input) { 'N' }

        it 'passes the input - no error is thrown' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end

      context 'when input is invalid - 2 integers and position not capitalized' do
        let(:user_input) { '0 0 n' }

        it 'passes the input - no error is thrown' do
          expect { subject }.to raise_error(Errors::InvalidInputError)
        end
      end
    end
  end
end
