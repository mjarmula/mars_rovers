require 'spec_helper'

RSpec.describe Stages::ReadingStage do
  describe '.start' do
    subject { described_class.start(rovers) }

    let(:rover) { double(Rover, report: report) }
    let(:rovers) { [rover] }
    let(:report) { '5 5 N' }

    it 'returns rovers report - current position' do
      expect(STDOUT).to receive(:puts).with(report)
      subject
    end
  end
end
