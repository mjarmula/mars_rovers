require 'spec_helper'

RSpec.describe Validators::RegexpValidator do
  describe '.validate!' do
    subject { described_class.validate!(params) }

    let(:params) { { subject: validator_subject, regexp: regexp } }
    let(:validator_subject) { double(String, match: match) }
    let(:regexp) { double('Regexp') }

    before do
      allow(validator_subject).to receive(:match).with(regexp).and_return(match)
    end

    context 'when the input is valid' do
      let(:match) { true }

      it 'doesnt raise an error' do
        expect { subject }.not_to raise_error
      end
    end

    context 'when the input is invalid' do
      let(:match) { nil }

      it 'throws invalid input error' do
        expect { subject }.to raise_error(Errors::InvalidInputError)
      end
    end
  end
end
