require 'spec_helper'

RSpec.describe Program do
  describe '.start' do
    subject { described_class.start(init_stage: init_stage) }

    let(:init_stage) { double(Stages::PlateauStage) }

    before do
      allow(init_stage).to receive(:start)
    end

    it 'starts first stage - plateau initialize' do
      expect(init_stage).to receive(:start)
      subject
    end
  end
end
