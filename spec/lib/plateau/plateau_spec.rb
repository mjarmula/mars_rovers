require 'spec_helper'

RSpec.describe Plateau do
  subject(:instance) { described_class.new(params) }

  let(:params) { { rows: rows, cols: cols } }
  let(:cols) { 1 }
  let(:rows) { 1 }
  let(:rover) { double(Rover, id: rover_id) }
  let(:rover_id) { '1' }
  let(:field1) { double(Plateau::Field, x: 0, y: 1) }
  let(:field2) { double(Plateau::Field, x: 0, y: 0) }
  let(:grid) do
    [
      [field1],
      [field2]
    ]
  end

  before do
    allow(instance).to receive(:grid).and_return(grid)
  end

  describe '#place_rover' do
    subject { instance.place_rover(place_rover_params) }

    let(:place_rover_params) { { rover: rover, x: x, y: y } }
    let(:x) { 0 }
    let(:y) { 0 }

    it 'places rover' do
      expect(Plateau::PlaceRover).to receive(:call).with(
        field: field2,
        rover: rover
      )
      subject
    end
  end

  describe '#find_field_by_position' do
    subject { instance.find_field_by_position(find_params) }

    context 'when field exists' do
      let(:find_params) { { x: 0, y: 0 } }

      it 'returns field' do
        is_expected.to eq(field2)
      end
    end

    context 'when field doesnt exist' do
      let(:find_params) { { x: 10, y: 0 } }

      it 'returns nil' do
        is_expected.to be_nil
      end
    end
  end
end
