require 'spec_helper'

describe Plateau::Field do
  subject(:instance) { described_class.new(params) }

  let(:params) { { x: x, y: y } }
  let(:x) { 5 }
  let(:y) { 5 }

  describe '#x' do
    subject { instance.x }

    it { is_expected.to eq(x) }
  end

  describe '#y' do
    subject { instance.y }

    it { is_expected.to eq(y) }
  end

  describe '#rover' do
    subject { instance.rover }

    before do
      expect(instance).to receive(:rover).and_return(rover)
    end

    context 'where rover is associated' do
      let(:rover) { double(Rover) }

      it { is_expected.to eq(rover) }
    end

    context 'where rover is not associated' do
      let(:rover) { nil }

      it { is_expected.to be_nil }
    end
  end
end
