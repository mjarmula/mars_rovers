require 'spec_helper'

RSpec.describe Plateau::PlaceRover do
  describe '.call' do
    subject { described_class.call(params) }

    let(:params) { { field: field, rover: rover } }
    let(:field) { double(Plateau::Field) }
    let(:rover) { double(Rover) }

    before do
      allow(rover).to receive(:field)
      allow(rover).to receive(:field=)
    end

    context 'field - rover connection' do
      before do
        allow(field).to receive(:rover).and_return(nil)
      end

      it 'connects field wih rover' do
        expect(field).to receive(:rover=).with(rover)
        subject
      end
    end

    context 'field existence validation' do
      context 'when field exists' do
        before do
          allow(field).to receive(:rover=)
          allow(field).to receive(:rover).and_return(nil)
        end

        it 'doesnt raise an error' do
          expect { subject }.not_to raise_error
        end
      end

      context 'when field doesnt exist' do
        let(:field) { nil }

        it 'throws undefined field error' do
          expect { subject }.to raise_error(Errors::UndefinedFieldError)
        end
      end
    end

    context 'field availability validation' do
      context 'when field is available' do
        before do
          allow(field).to receive(:rover=)
          allow(field).to receive(:rover).and_return(nil)
        end

        it 'doesnt raise an error' do
          expect { subject }.not_to raise_error
        end
      end
    end

    context 'when field is not available' do
      before do
        allow(field).to receive(:rover).and_return(rover)
      end

      it 'throws taken field error' do
        expect { subject }.to raise_error(Errors::TakenFieldError)
      end
    end
  end
end
